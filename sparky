#!/bin/bash
# 
# Sparkcore local build utility  
# Copyright (C) 2013-2014 by Xose Pérez <xose dot perez at gmail dot com>
# http://tinkerman.eldiariblau.net
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=0.1

# Usage
usage() {
    echo "Sparkcore local build utility v$VERSION"
    echo ""
    echo "$0 <init|update|make|upload|reset|join> [options]"
    echo "" 
}

do_link() {
    for folder in src inc; do
        find $folder -type f -exec ln -fs $PWD/{} $SPARKCORE_FOLDER/core-firmware/{} \;
        find $folder -type l -exec ln -fs $PWD/{} $SPARKCORE_FOLDER/core-firmware/{} \;
    done
}

do_unlink() {
    pushd $SPARKCORE_FOLDER/core-firmware/ >> /dev/null
    find . -type l -exec rm {} \;
    git checkout -- .
    popd >> /dev/null
}

do_update() {
    pushd $SPARKCORE_FOLDER/core-firmware/ >> /dev/null
    echo -n "[core-firmware] " ; git pull
    popd >> /dev/null
    pushd $SPARKCORE_FOLDER/core-common-lib/ >> /dev/null
    echo -n "[core-common-lib] " ; git pull
    popd >> /dev/null
    pushd $SPARKCORE_FOLDER/core-communication-lib/ >> /dev/null
    echo -n "[core-communication-lib] " ; git pull
    popd >> /dev/null
}

do_make() {
    pushd $SPARKCORE_FOLDER/core-firmware/build >> /dev/null
    make $@ || exit 5
    popd >> /dev/null
}

do_upload() {
    pushd $SPARKCORE_FOLDER/core-firmware/build >> /dev/null
    sudo dfu-util -d 1d50:607f -a 0 -s 0x08005000:leave -D core-firmware.bin || exit 5
    popd >> /dev/null
}

do_join() {

    echo ""

    echo "//-------------------------------------"
    echo "// HEADER FILES"
    echo "//-------------------------------------"
    echo ""
    find . -name "*.h" -exec cat {} \;

    echo ""
    echo "//-------------------------------------"
    echo "// LIBRARIES"
    echo "//-------------------------------------"
    echo ""
    find . -name "*.cpp" -not -name application.cpp -exec cat {} \;

    echo ""
    echo "//-------------------------------------"
    echo "// APPLICATION FILE"
    echo "//-------------------------------------"
    echo ""
    cat src/application.cpp

}

do_init() {
    mkdir -p src inc
    cat > src/application.cpp <<DELIM
#include "application.h"

void setup() {

}

void loop() {

}
DELIM
}

# check option parameter
if [ $# -lt 1 ]; then
    usage
    exit 1
fi

option=$1
shift

# check firmware folder
if [ "$SPARKCORE_FOLDER" == "" ]; then
    echo "Error: SPARKCORE_FOLDER environmental variable not defined"
    exit 2
fi

if [ ! -d $SPARKCORE_FOLDER ]; then
    echo "Error: $SPARKCORE_FOLDER does not exist"
    exit 2
fi

if [ ! -f $SPARKCORE_FOLDER/core-firmware/src/application.cpp ]; then
    echo "Error: $SPARKCORE_FOLDER/core-firmware/src/application.cpp does not exist"
    exit 2
fi

# check origin folder
if [ "$option" != "init" ]; then
    if [ ! -f src/application.cpp ]; then
        echo "Error: src/application.cpp does not exist"
        exit 3
    fi
fi

# options
case "$option" in
    "init")     do_init ;;
    "link")     do_link ;; # useful when linking more than one project (for instance library projects)
    "unlink")   do_unlink ;; # deprecated (use reset)
    "update")   do_update ;;
    "make")     do_link ; do_make $@ ;;
    "upload")   do_upload ;;
    "reset")    do_unlink ; do_update ; do_make clean ;;
    "join")     do_join ;;
    *)          echo "Error: unknown option $option" ; usage; exit 4 ;;
esac
