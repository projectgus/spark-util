# Spark-util

Utility to help building Sparkcore firmware locally.

## Motivation

The goal is to avoid duplicating the core-firmware repository for every application,
keep the application repository relatively simple and have an easy way to build and upload code to the board.
Everything from the command line.

## What does it do

Basically it softlinks the content of your project to the core-firmware folder, 
overwriting whatever files there might already be there (like `application.cpp`, off course).
This process is called "linking" and its transparent.

You can build you project with the "make" option from your project root folder, no need to go to core-firmware/build.
Before invoking gnu make utility it will create softlinks for all the files in your project in the core-firmware folder.
You can pass make whatever params you want (like `make clean`).

The you can upload your code using the "upload" option.

Finally, you "unlink" your project, which efectively removes the softlink to you project from the core-firmware repo
and performs a `git checkout -- .` to reset its state.

## Installation

### 1. Configure your local environment

Follow the instructions from [Sparkcore firmware library](https://github.com/spark/core-firmware).
Make sure you checkout the three core repos into the same folder, otherwise it won't build.
Also, make sure you have dfu-util 0.7 minimum (type `dfu-util -v` to get the current installed version)

### 2. Set the SPARKCORE_FOLDER environmental variable to the folder where the three repos are. 

Add it to your .bashrc or .profile to have it set for every terminal.

### 3. Copy the "sparky" script somewhere in your PATH.

## Usage

The script has to be executed from the code root (it looks for src/applicacion.cpp relatively to the current path).

### sparky reset

Resets the core-firmware repository, removing any links to you projects, checkout out the code and cleaning the build folder.
It is recommended to perform a reset when changing to a new project.

### sparky init

Creates a new project structure in a blank folder, like this:

```
project
    src
        application.cpp
    inc
```

With a basic scafolding in the application.cpp file.

### sparky link

Link the current project files into the core-firmware folder. Not needed if you are working on a single project since `sparky make` already
links the project prior to build it, but can be useful if you want to link more projects, for instance library projects.

### sparky make [option]

Invokes "make" on the build folder of the core-firmware folder. You can pass whatever params to make (like `sparky make clean`).

### sparky upload

Uploads the latest binary to the connected board. It invokes dfu-util to do so.

### sparky update

Updates the three sparkcore repositories.

### sparky join

Joins all files from your project into one single stream which is output through stdout.
This is intended to get a single file from you code to use it from the online IDE.

```
sparky join > complete.cpp
```

## Notes

The simplest project is to have a single application.cpp file under the `src` folder in your project.
You can also add other .cpp files to the `src` folder and .h header files to the `inc` folder.
If you are adding new cpp files aside `application.cpp` you will need to overwrite the `build.mk` file from the
`core-firmware/src` folder, adding your files to the bottom of the CPPSRC list.

Remember, YOUR files will overwrite the original ones.

## Use case

Imagine I'm starting a project that will involve a DHT22 sensor to monitor himidity and temperature.
I will start with something as simple as this:

```
cd project
sparky init
```

That will create this structure

```
project
    src
        application.cpp
    inc
```

Now I will add some basic functionality and try to build it:

```
sparky reset     // clean the core-firmware folder
sparky make      // link the files and build the project
sparky upload    // have it deployed to the board
```

Now I add a library to manage the DHT22, I will add some files to my project folder.

```
project
    src
        application.cpp
        dht22.cpp
        build.mk
    inc
        dht22.h
```

Note the `build.mk` file which is a copy of the original one adding the requierement to compile the `dht22.cpp` file.
Now when I try to build the project again it will create softlinks for any new files before.

```
sparky make      // it will link any new files and build the project
sparky upload    // and deploy again
```

Once I finish developing it's good to do some cleanup

```
sparky reset
```

## License

Copyright (C) 2013-2014 by Xose Pérez <xose dot perez at gmail dot com>
<http://tinkerman.eldiariblau.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

